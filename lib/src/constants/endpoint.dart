class Endpoint {
  static const baseUrl = 'http://develop-at.vesperia.id:1091/api/v1';
  static const login = '/sign-in';
  static const logout = '/sign-out';
  static const getUser = '/user';
  static const getProduct = '/product';
  static const getProductRating = '/rating';
}