import 'package:dio/dio.dart';
import 'package:entrance_test/src/models/response/product_detail_response_model.dart';
import 'package:get_storage/get_storage.dart';

import '../constants/endpoint.dart';
import '../constants/local_data_key.dart';
import '../models/request/product_list_request_model.dart';
import '../models/response/product_list_response_model.dart';
import '../models/response/product_rating_response_model.dart';
import '../utils/networking_util.dart';

class ProductRepository {
  final Dio _client;
  final GetStorage _local;

  ProductRepository({required Dio client, required GetStorage local})
      : _client = client,
        _local = local;

  Future<ProductListResponseModel> getProductList(
      ProductListRequestModel request) async {
    try {
      String endpoint = Endpoint.getProduct;
      final responseJson = await _client.get(
        endpoint,
        data: request,
        options: NetworkingUtil.setupNetworkOptions(
          'Bearer ${_local.read(LocalDataKey.token)}',
        ),
      );
      return ProductListResponseModel.fromJson(responseJson.data);
    } on DioError catch (_) {
      rethrow;
    }
  }

  Future<ProductDetailResponseModel> getProductDetail(String id) async {
    try {
      String endpoint = Endpoint.getProduct;
      final responseJson = await _client.get(
        '$endpoint/$id',
        options: NetworkingUtil.setupNetworkOptions(
          'Bearer ${_local.read(LocalDataKey.token)}',
        ),
      );
      return ProductDetailResponseModel.fromJson(responseJson.data);
    } on DioError catch (_) {
      rethrow;
    }
  }

  Future<ProductRatingResponseModel> getProductRatingList(
    String productId,
  ) async {
    try {
      String endpoint = Endpoint.getProductRating;
      final responseJson = await _client.get(
        endpoint,
        queryParameters: {
          'product_id': productId,
          'page': 1,
          'limit': 3,
        },
        options: NetworkingUtil.setupNetworkOptions(
          'Bearer ${_local.read(LocalDataKey.token)}',
        ),
      );
      return ProductRatingResponseModel.fromJson(responseJson.data);
    } on DioError catch (_) {
      rethrow;
    }
  }
}
