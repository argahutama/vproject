import 'package:dio/dio.dart';
import 'package:entrance_test/src/constants/local_data_key.dart';
import 'package:entrance_test/src/models/request/login_request_model.dart';
import 'package:entrance_test/src/models/response/login_response_model.dart';
import 'package:flutter_file_downloader/download_callbacks.dart';
import 'package:flutter_file_downloader/flutter_file_downloader.dart';
import 'package:get_storage/get_storage.dart';

import '../constants/endpoint.dart';
import '../models/response/logout_response_model.dart';
import '../models/response/user_response_model.dart';
import '../utils/networking_util.dart';

class UserRepository {
  final Dio _client;
  final GetStorage _local;

  UserRepository({required Dio client, required GetStorage local})
      : _client = client,
        _local = local;

  Future<LoginResponseModel> login(LoginRequestModel request) async {
    try {
      final responseJson = await _client.post(
        Endpoint.login,
        data: request,
        options: NetworkingUtil.setupNetworkOptions(null),
      );
      final response = LoginResponseModel.fromJson(responseJson.data);
      _local.write(LocalDataKey.token, response.data.token);
      return response;
    } on DioException catch (_) {
      rethrow;
    }
  }

  Future<LogoutResponseModel> logout() async {
    try {
      final responseJson = await _client.post(
        Endpoint.logout,
        options: NetworkingUtil.setupNetworkOptions(
          'Bearer ${_local.read(LocalDataKey.token)}',
        ),
      );
      final response = LogoutResponseModel.fromJson(responseJson.data);
      _local.remove(LocalDataKey.token);
      return response;
    } on DioException catch (_) {
      rethrow;
    }
  }

  Future<UserResponseModel> getUser() async {
    try {
      final responseJson = await _client.get(
        Endpoint.getUser,
        options: NetworkingUtil.setupNetworkOptions(
            'Bearer ${_local.read(LocalDataKey.token)}'),
      );
      final model = UserResponseModel.fromJson(responseJson.data);
      return model;
    } on DioException catch (_) {
      rethrow;
    }
  }

  bool doesTokenExist() {
    try {
      final String? token = _local.read(LocalDataKey.token);
      return token?.isNotEmpty ?? false;
    } on DioException catch (_) {
      return false;
    }
  }

  /*
    This Function is used as challenge tester
    DO NOT modify this function
   */
  Future<void> testUnauthenticated() async {
    try {
      final realToken = _local.read<String?>(LocalDataKey.token);
      await _local.write(
          LocalDataKey.token, '619|kM5YBY5yM15KEuSmSMaEzlfv0lWs83r4cp4oty2T');
      getUser();
      //401 not caught as exception
      await _local.write(LocalDataKey.token, realToken);
    } on DioException catch (_) {
      rethrow;
    }
  }

  void downloadFile(
    OnDownloadCompleted onCompleted,
    OnDownloadError onError,
  ) {
    FileDownloader.downloadFile(
      url: 'https://www.tutorialspoint.com/flutter/flutter_tutorial.pdf',
      onDownloadCompleted: onCompleted,
      onDownloadError: onError,
    );
  }

  bool isOnBoardingShown() {
    final bool? isOnBoardingShown = _local.read(LocalDataKey.onBoardingShown);
    return isOnBoardingShown ?? false;
  }

  Future<void> setOnBoardingShown(bool value) async {
    _local.write(LocalDataKey.onBoardingShown, value);
  }
}
