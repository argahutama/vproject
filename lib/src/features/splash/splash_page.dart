import 'package:entrance_test/src/constants/icon.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants/color.dart';
import 'component/splash_controller.dart';

class SplashPage extends GetView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          body: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  white,
                  primary,
                ],
              ),
            ),
            child: Center(
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset(app_icon),
                  ),
                  Obx(
                    () => Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: controller.appVersionName.isNotEmpty
                            ? Text('Ver. ${controller.appVersionName}')
                            : const SizedBox(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
