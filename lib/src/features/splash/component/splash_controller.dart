import 'package:entrance_test/src/repositories/user_repository.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../../../app/routes/route_name.dart';

class SplashController extends GetxController {
  final UserRepository _userRepository;

  final _appVersionName = "".obs;

  String get appVersionName => _appVersionName.value;

  SplashController({
    required UserRepository userRepository,
  }) : _userRepository = userRepository;

  @override
  void onInit() {
    super.onInit();
    _fetchAppVersionName();
    _navigateToNextPage();
  }

  void _navigateToNextPage() async {
    Future.delayed(const Duration(seconds: 3), () {
      if (_userRepository.doesTokenExist()) {
        Get.offAllNamed(RouteName.dashboard);
      } else if (_userRepository.isOnBoardingShown()) {
        Get.offAllNamed(RouteName.login);
      } else {
        Get.offAllNamed(RouteName.onBoarding);
      }
    });
  }

  Future<void> _fetchAppVersionName() async {
    final info = await PackageInfo.fromPlatform();
    _appVersionName.value = info.version;
  }
}
