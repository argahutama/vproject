import 'package:entrance_test/src/repositories/user_repository.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../app/routes/route_name.dart';
import '../../../../utils/networking_util.dart';
import '../../../../widgets/snackbar_widget.dart';

class ProfileController extends GetxController {
  final UserRepository _userRepository;

  final _name = "".obs;

  String get name => _name.value;

  final _phone = "".obs;

  String get phone => _phone.value;

  final _profilePictureUrl = "".obs;

  String get profilePictureUrl => _profilePictureUrl.value;

  ProfileController({
    required UserRepository userRepository,
  }) : _userRepository = userRepository;

  @override
  void onInit() {
    super.onInit();
    loadUserFromServer();
  }

  void loadUserFromServer() async {
    try {
      final response = await _userRepository.getUser();
      if (response.status == 0) {
        final localUser = response.data;

        _name.value = localUser.name;
        _phone.value = localUser.countryCode.isNotEmpty
            ? "+${localUser.countryCode}${localUser.phone}"
            : "";
        _profilePictureUrl.value = localUser.profilePicture ?? '';
      } else {
        SnackbarWidget.showFailedSnackbar(response.message);
      }
    } catch (error) {
      SnackbarWidget.showFailedSnackbar(NetworkingUtil.errorMessage(error));
    }
  }

  onEditProfileClick() async {
    Get.toNamed(RouteName.editProfile);
  }

  /*
    This Function is used as challenge tester
    DO NOT modify this function
   */
  onTestUnauthenticatedClick() async {
    await _userRepository.testUnauthenticated();
  }

  void onDownloadFileClick() async {
    _userRepository.downloadFile(
      _onDownloadSuccess,
      _onDownloadError,
    );
  }

  void _onDownloadSuccess(String path) {
    SnackbarWidget.showSuccessSnackbar('Download success! Path: $path');
  }

  void _onDownloadError(String message) {
    SnackbarWidget.showFailedSnackbar(message);
  }

  void onOpenWebPageClick() async {
    launchUrl(
      Uri.parse('https://www.youtube.com/watch?v=lpnKWK-KEYs'),
      mode: LaunchMode.inAppWebView,
    );
  }

  void doLogout() async {
    try {
      final response = await _userRepository.logout();

      if (response.status == 0) {
        Get.offAllNamed(RouteName.login);
      } else {
        SnackbarWidget.showFailedSnackbar(response.message);
      }
    } catch (error) {
      SnackbarWidget.showFailedSnackbar(NetworkingUtil.errorMessage(error));
    }
  }
}
