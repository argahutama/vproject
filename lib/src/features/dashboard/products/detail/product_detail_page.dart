import 'package:cached_network_image/cached_network_image.dart';
import 'package:entrance_test/src/models/product_model.dart';
import 'package:entrance_test/src/models/product_rating_model.dart';
import 'package:entrance_test/src/utils/number_ext.dart';
import 'package:entrance_test/src/utils/string_ext.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constants/color.dart';
import '../../../../constants/icon.dart';
import '../../../../constants/image.dart';
import 'component/product_detail_controller.dart';

class ProductDetailPage extends GetWidget<ProductDetailController> {
  const ProductDetailPage({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: white,
        appBar: AppBar(
          backgroundColor: white,
          title: const Text(
            "Detail Product",
            style: TextStyle(
              fontSize: 16,
              color: gray900,
              fontWeight: FontWeight.w600,
            ),
          ),
          elevation: 0,
        ),
        body: _buildProductDetailSection(context),
      );

  Widget _buildProductDetailSection(BuildContext context) => Obx(
        () {
          final productDetail = controller.productDetail;
          final ratingList = controller.productRatingList;

          return controller.isLoadingRetrieveProduct
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CachedNetworkImage(
                        width: MediaQuery.sizeOf(context).width,
                        imageUrl: productDetail?.images?.firstOrNull?.url ?? '',
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) => Image.asset(
                          defaultProfileImage,
                          fit: BoxFit.cover,
                        ),
                      ),
                      const SizedBox(height: 16),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: Text(
                          productDetail?.name ?? '',
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                            fontSize: 18,
                            color: gray900,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: productDetail?.discountPrice !=
                                productDetail?.price
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    productDetail?.price.inRupiah() ?? '',
                                    textAlign: TextAlign.start,
                                    style: const TextStyle(
                                      fontSize: 16,
                                      color: gray600,
                                      fontWeight: FontWeight.w400,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  Text(
                                    productDetail?.discountPrice.inRupiah() ??
                                        '',
                                    textAlign: TextAlign.start,
                                    style: const TextStyle(
                                      fontSize: 18,
                                      color: red600,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              )
                            : Text(
                                productDetail?.price.inRupiah() ?? '',
                                textAlign: TextAlign.start,
                                style: const TextStyle(
                                  fontSize: 18,
                                  color: gray900,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                      ),
                      const SizedBox(height: 8),
                      (productDetail?.ratingCount ?? 0) == 0
                          ? const SizedBox.shrink()
                          : Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(ic_product_rating),
                                  const SizedBox(width: 4),
                                  Text(
                                    productDetail?.ratingAverage ?? '',
                                    textAlign: TextAlign.start,
                                    style: const TextStyle(
                                      fontSize: 14,
                                      color: gray900,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  Text(
                                    '(${productDetail?.ratingCount} Review${(productDetail?.ratingCount ?? 0) == 0 ? '' : 's'})',
                                    textAlign: TextAlign.start,
                                    style: const TextStyle(
                                      fontSize: 14,
                                      color: gray600,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      const SizedBox(height: 16),
                      _buildSeparator(),
                      ..._buildProductDescriptionWidgets(productDetail),
                      ..._buildProductRefundCondition(productDetail),
                      ..._buildProductReviewWidgets(productDetail, ratingList),
                    ],
                  ),
                );
        },
      );

  List<Widget> _buildProductRefundCondition(
    ProductModel? productDetail,
  ) =>
      productDetail?.returnTerms.isNullOrEmpty ?? true
          ? []
          : [
              const SizedBox(height: 24),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  'Terms & Conditions of Return / Refund',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 16,
                    color: gray900,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  productDetail?.returnTerms ?? '',
                  textAlign: TextAlign.start,
                  style: const TextStyle(
                    fontSize: 12,
                    color: gray900,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              const SizedBox(height: 28),
              _buildSeparator(),
            ];

  List<Widget> _buildProductDescriptionWidgets(
    ProductModel? productDetail,
  ) =>
      productDetail?.description.isNullOrEmpty ?? true
          ? []
          : [
              const SizedBox(height: 24),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  'Product Description',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 16,
                    color: gray900,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  productDetail?.description ?? '',
                  textAlign: TextAlign.start,
                  style: const TextStyle(
                    fontSize: 12,
                    color: gray900,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              const SizedBox(height: 28),
              _buildSeparator(),
            ];

  List<Widget> _buildProductReviewWidgets(
    ProductModel? productDetail,
    List<ProductRatingModel> ratingList,
  ) =>
      ratingList.isEmpty
          ? []
          : [
              const SizedBox(height: 24),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Product Review',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16,
                          color: gray900,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Text(
                      'See More',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 14,
                        color: primary,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      ic_product_rating,
                      width: 20,
                      height: 20,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      productDetail?.ratingAverage ?? '',
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                        fontSize: 14,
                        color: gray900,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    const SizedBox(width: 4),
                    Text(
                      'from ${productDetail?.ratingCount} rating • ${productDetail?.reviewCount} review${(productDetail?.ratingCount ?? 0) == 0 ? '' : 's'}',
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                        fontSize: 14,
                        color: gray600,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 24),
              ...ratingList.map(_buildRatingView).toList()
            ];

  Widget _buildRatingView(ProductRatingModel rating) {
    final now = DateTime.now();
    final ratingCreatedAt = rating.createdAt;
    final gapInDays = (ratingCreatedAt?.difference(now).inDays ?? 0).abs();
    final ratingCratedAtText = '$gapInDays day${gapInDays > 1 ? 's' : ''} ago';

    return Padding(
      padding: const EdgeInsets.fromLTRB(24, 0, 24, 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipOval(
                child: CachedNetworkImage(
                  width: 48,
                  height: 48,
                  imageUrl: rating.user?.profilePicture ?? '',
                  fit: BoxFit.cover,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      CircularProgressIndicator(
                    value: downloadProgress.progress,
                  ),
                  errorWidget: (context, url, error) => Image.asset(
                    width: 48,
                    height: 48,
                    defaultProfileImage,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              const SizedBox(width: 12),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      rating.user?.name ?? '',
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                        fontSize: 14,
                        color: gray900,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    const SizedBox(height: 4),
                    Row(
                      children: List.generate(
                        5,
                        (index) => Image.asset(
                          ic_product_rating,
                          color: index + 1 <= (rating.rating ?? 0)
                              ? yellow600
                              : gray200,
                          width: 16,
                          height: 16,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Text(
                ratingCratedAtText,
                textAlign: TextAlign.start,
                style: const TextStyle(
                  fontSize: 12,
                  color: gray900,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
          const SizedBox(height: 12),
          Text(
            rating.review ?? '',
            textAlign: TextAlign.start,
            style: const TextStyle(
              fontSize: 12,
              color: gray900,
              fontWeight: FontWeight.w400,
            ),
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }

  Widget _buildSeparator() => Container(
        color: gray100,
        height: 4,
      );
}
