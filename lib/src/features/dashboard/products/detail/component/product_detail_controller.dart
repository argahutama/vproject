import 'package:entrance_test/src/models/product_model.dart';
import 'package:entrance_test/src/models/product_rating_model.dart';
import 'package:entrance_test/src/repositories/product_repository.dart';
import 'package:get/get.dart';

import '../../../../../utils/argument_key.dart';
import '../../../../../utils/networking_util.dart';
import '../../../../../widgets/snackbar_widget.dart';

class ProductDetailController extends GetxController {
  final ProductRepository _productRepository;

  final _isLoadingRetrieveProduct = false.obs;

  bool get isLoadingRetrieveProduct => _isLoadingRetrieveProduct.value;

  String get _productId => Get.arguments[ArgumentKey.id];

  final _productDetail = Rx<ProductModel?>(null);

  ProductModel? get productDetail => _productDetail.value;

  final _productRatingList = Rx<List<ProductRatingModel>>([]);

  List<ProductRatingModel> get productRatingList => _productRatingList.value;

  ProductDetailController({
    required ProductRepository productRepository,
  }) : _productRepository = productRepository;

  @override
  void onInit() {
    _getProductDetail();
    _getProductRatingList();
    super.onInit();
  }

  void _getProductDetail() async {
    _isLoadingRetrieveProduct.value = true;

    try {
      final productDetailResponse =
          await _productRepository.getProductDetail(_productId);
      _productDetail.value = productDetailResponse.data;
    } catch (error) {
      SnackbarWidget.showFailedSnackbar(NetworkingUtil.errorMessage(error));
    }

    _isLoadingRetrieveProduct.value = false;
  }

  void _getProductRatingList() async {
    _isLoadingRetrieveProduct.value = true;

    try {
      final productRatingListResponse =
          await _productRepository.getProductRatingList(_productId);
      _productRatingList.value = productRatingListResponse.data;
    } catch (error) {
      SnackbarWidget.showFailedSnackbar(NetworkingUtil.errorMessage(error));
    }

    _isLoadingRetrieveProduct.value = false;
  }
}
