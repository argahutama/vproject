import 'package:entrance_test/src/features/onboarding/component/on_boarding_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants/color.dart';
import '../../widgets/button_icon.dart';

class OnBoardingPage extends GetView<OnBoardingController> {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: white,
        body: PageView.builder(
          controller: controller.pageController,
          itemCount: controller.items.length,
          itemBuilder: (context, index) => buildOnBoardingItem(context, index),
          physics: const NeverScrollableScrollPhysics(),
        ),
      );

  Widget buildOnBoardingItem(BuildContext context, int index) {
    final item = controller.items[index];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Image.asset(
          item.image,
          fit: BoxFit.cover,
        ),
        Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              Text(
                item.title,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 16,
                  color: gray900,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(height: 4),
              Text(
                item.content,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 12,
                  color: gray600,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
            controller.items.length,
            (index) => _indicator(index <= controller.currentIndex),
          ),
        ),
        const Spacer(),
        Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                onPressed: () {
                  controller.skip();
                },
                child: const Text('Skip'),
              ),
              ButtonIcon(
                onClick: index == controller.items.length - 1
                    ? controller.finish
                    : controller.next,
                textLabel:
                    index == controller.items.length - 1 ? 'Finish' : 'Next',
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _indicator(bool isActive) => SizedBox(
        height: 10,
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 150),
          margin: const EdgeInsets.symmetric(horizontal: 4.0),
          height: isActive ? 10 : 8.0,
          width: isActive ? 12 : 8.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: isActive ? primary : gray600,
          ),
        ),
      );
}
