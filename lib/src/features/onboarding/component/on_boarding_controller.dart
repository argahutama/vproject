import 'package:entrance_test/src/constants/image.dart';
import 'package:entrance_test/src/repositories/user_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../models/on_boarding_item.dart';

class OnBoardingController extends GetxController {
  final UserRepository _userRepository;
  late PageController pageController;

  final List<OnBoardingItem> items = List.generate(
    4,
    (index) => OnBoardingItem(
      image: defaultProfileImage,
      title: 'Title $index',
      content:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nLorem ipsum dolor sit amet, consectetur adipiscing elit.\nLorem ipsum dolor sit amet, consectetur adipiscing elit.',
    ),
  );

  final _currentIndex = 0.obs;

  int get currentIndex => _currentIndex.value;

  OnBoardingController({
    required UserRepository userRepository,
  }) : _userRepository = userRepository;

  @override
  void onInit() {
    super.onInit();
    pageController = PageController(initialPage: _currentIndex.value);
  }

  @override
  void onClose() {
    pageController.dispose();
    super.onClose();
  }

  void next() {
    if (_currentIndex.value < items.length - 1) {
      _currentIndex.value++;
      pageController.animateToPage(
        currentIndex,
        curve: Curves.easeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
  }

  void skip() {
    Get.offNamed('/login');
    _userRepository.setOnBoardingShown(true);
  }

  void finish() {
    Get.offNamed('/login');
    _userRepository.setOnBoardingShown(true);
  }
}
