import 'package:dio/dio.dart';
import 'package:entrance_test/src/features/onboarding/component/on_boarding_controller.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../repositories/user_repository.dart';
import '../../../utils/auth_interceptor.dart';

class OnBoardingBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(UserRepository(
      client: Get.find<Dio>()..interceptors.add(Get.find<AuthInterceptor>()),
      local: Get.find<GetStorage>(),
    ));

    Get.put(OnBoardingController(userRepository: Get.find<UserRepository>()));
  }
}
