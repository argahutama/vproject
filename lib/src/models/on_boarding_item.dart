class OnBoardingItem {
  final String image;
  final String title;
  final String content;

  OnBoardingItem({
    required this.image,
    required this.title,
    required this.content,
  });
}