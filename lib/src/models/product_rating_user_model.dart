class ProductRatingUserModel {
  final String? name;
  final String? email;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? id;
  final String? phoneNumber;
  final String? profilePicture;
  final String? role;
  final String? description;
  final String? gender;
  final String? csPhoneNumber;
  final int? height;
  final int? weight;
  final bool? isReady;
  final bool? isAllProduct;
  final String? countryCode;
  final int? totalClinicTransaction;
  final int? totalOnlineStoreTransaction;
  final int? totalAmountClinicTransaction;
  final int? totalAmountOnlineStoreTransaction;

  ProductRatingUserModel({
    this.name,
    this.email,
    this.createdAt,
    this.updatedAt,
    this.id,
    this.phoneNumber,
    this.profilePicture,
    this.role,
    this.description,
    this.gender,
    this.csPhoneNumber,
    this.height,
    this.weight,
    this.isReady,
    this.isAllProduct,
    this.countryCode,
    this.totalClinicTransaction,
    this.totalOnlineStoreTransaction,
    this.totalAmountClinicTransaction,
    this.totalAmountOnlineStoreTransaction,
  });

  factory ProductRatingUserModel.fromJson(Map<String, dynamic> json) =>
      ProductRatingUserModel(
        name: json["name"],
        email: json["email"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        id: json["id"],
        phoneNumber: json["phone_number"],
        profilePicture: json["profile_picture"],
        role: json["role"],
        description: json["description"],
        gender: json["gender"],
        csPhoneNumber: json["cs_phone_number"],
        height: json["height"],
        weight: json["weight"],
        isReady: json["is_ready"],
        isAllProduct: json["is_all_product"],
        countryCode: json["country_code"],
        totalClinicTransaction: json["total_clinic_transaction"],
        totalOnlineStoreTransaction: json["total_online_store_transaction"],
        totalAmountClinicTransaction: json["total_amount_clinic_transaction"],
        totalAmountOnlineStoreTransaction:
            json["total_amount_online_store_transaction"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "id": id,
        "phone_number": phoneNumber,
        "profile_picture": profilePicture,
        "role": role,
        "description": description,
        "gender": gender,
        "cs_phone_number": csPhoneNumber,
        "height": height,
        "weight": weight,
        "is_ready": isReady,
        "is_all_product": isAllProduct,
        "country_code": countryCode,
        "total_clinic_transaction": totalClinicTransaction,
        "total_online_store_transaction": totalOnlineStoreTransaction,
        "total_amount_clinic_transaction": totalAmountClinicTransaction,
        "total_amount_online_store_transaction":
            totalAmountOnlineStoreTransaction,
      };
}
