import 'package:entrance_test/src/models/product_rating_model.dart';

class ProductRatingResponseModel {
  ProductRatingResponseModel({
    required this.status,
    required this.message,
    required this.data,
  });

  final int status;
  final String message;
  final List<ProductRatingModel> data;

  factory ProductRatingResponseModel.fromJson(Map<String, dynamic> json) =>
      ProductRatingResponseModel(
        status: json['status'],
        message: json['message'],
        data: List<ProductRatingModel>.from(
          json['data'].map((x) => ProductRatingModel.fromJson(x)),
        ),
      );

  Map<String, dynamic> toJson() => {
        'status': status,
        'message': message,
        'data': data,
      };
}
