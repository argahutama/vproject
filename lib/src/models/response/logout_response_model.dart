class LogoutResponseModel {
  LogoutResponseModel({
    required this.status,
    required this.message,
  });

  final int status;
  final String message;

  factory LogoutResponseModel.fromJson(Map<String, dynamic> json) =>
      LogoutResponseModel(
        status: json['status'],
        message: json['message'],
      );

  Map<String, dynamic> toJson() => {
        'status': status,
        'message': message,
      };
}
