class LoginRequestModel {
  LoginRequestModel({
    required String countryCode,
    required String phoneNumber,
    required String password,
  })  : _countryCode = countryCode,
        _phoneNumber = phoneNumber,
        _password = password;

  final String _countryCode;
  final String _phoneNumber;
  final String _password;

  Map<String, dynamic> toJson() => {
        'country_code': _countryCode,
        'phone_number': _phoneNumber,
        'password': _password,
      };

  @override
  String toString() {
    return toJson().toString();
  }
}
