import 'product_rating_user_model.dart';

class ProductRatingModel {
  final String? id;
  final DateTime? createdAt;
  final String? review;
  final int? rating;
  final bool? isAnonymous;
  final String? status;
  final String? productName;
  final String? invoiceNumber;
  final ProductRatingUserModel? user;

  ProductRatingModel({
    this.id,
    this.createdAt,
    this.review,
    this.rating,
    this.isAnonymous,
    this.status,
    this.productName,
    this.invoiceNumber,
    this.user,
  });

  factory ProductRatingModel.fromJson(Map<String, dynamic> json) =>
      ProductRatingModel(
        id: json["id"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        review: json["review"],
        rating: json["rating"],
        isAnonymous: json["is_anonymous"],
        status: json["status"],
        productName: json["product_name"],
        invoiceNumber: json["invoice_number"],
        user: json["user"] == null
            ? null
            : ProductRatingUserModel.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "created_at": createdAt?.toIso8601String(),
        "review": review,
        "rating": rating,
        "is_anonymous": isAnonymous,
        "status": status,
        "product_name": productName,
        "invoice_number": invoiceNumber,
        "user": user?.toJson(),
      };
}
