import 'package:dio/dio.dart';
import 'package:get/get.dart' as getx;
import 'package:get_storage/get_storage.dart';

import '../../app/routes/route_name.dart';
import '../constants/local_data_key.dart';

class AuthInterceptor extends Interceptor {
  final GetStorage _local;

  AuthInterceptor({required GetStorage local}) : _local = local;

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (err.response?.statusCode == 401 &&
        err.requestOptions.path != '/sign-in') {
      _local.remove(LocalDataKey.token);
      getx.Get.offAllNamed(RouteName.login);
    }
    super.onError(err, handler);
  }
}
