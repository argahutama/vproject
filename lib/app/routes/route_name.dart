abstract class RouteName {
  static const splash = '/splash';
  static const onBoarding = '/on-boarding';
  static const login = '/login';
  static const dashboard = "/dashboard";
  static const productDetail = "/product-detail";

  static const editProfile = '/profile/edit';
}
